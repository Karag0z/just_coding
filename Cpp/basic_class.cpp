/*
 * Classes
 *
 * Ref: https://www.udacity.com/course/c-for-programmers--ud210
**/

#include <iostream>

using namespace std;

class Cat
{   // Following ones are private although they are not stated
    string name;
    int age;
public:
    /* Declare if it is defined outside of the class*/
    // Cat(); //declaring the constructor
    // ~Cat(); //declaring the deconstructor
    // void setName(string nameIn);
    // void setAge(int ageIn);
    // string getName();
    int getAge();
    void printInfo();
    void compareAge(Cat catIn);

    // Constructor for initial values, you can use parameters for initialization
    Cat()
    {
        name = "Initial name";
        age = -1;
    }
    // Deconstructor
    ~Cat()
    {   // Last created deleted first
        cout << "Deleting the cat " << name <<"\n";
    }

    void setName(string nameIn)
    {
        name = nameIn;
    }

    void setAge(int ageIn)
    {
        age = ageIn;
    }

    string getName()
    {
        return name;
    }
};

// Public functions can be defined here with ClassName::functionName()
int Cat::getAge()
{
    return age;
}

void Cat::printInfo()
{
    cout << name << ": " << age <<endl;
}

void Cat::compareAge(Cat catIn)
{
    if (this->getAge() > catIn.getAge())
    {
        cout<< this->getName() <<" is older than "<< catIn.getName() <<"\n";
    }
    else if (this->getAge() < catIn.getAge())
    {
        cout<< this->getName() <<" is younger than "<< catIn.getName() <<"\n";
    }
    else
    {
        cout<< this->getName() <<" and "<< catIn.getName() <<" are the same age.\n";
    }
}

int main()
{
    Cat cat1, cat2;
    cat1.setName("Pamuk");
    cat2.setName("Zeybek");
    cat1.setAge(7);
    cat2.setAge(5);

    cat1.printInfo();
    cat2.printInfo();
    cat1.compareAge(cat2);

    return 0;
}
