/*
 * Linked lists
 *
 * Ref: https://www.hackerrank.com/challenges/30-linked-list
**/

#include <iostream>
#include <cstddef>
using namespace std;

class Node
{
    public:
        int data;
        Node *next;
        Node(int d)
        {
            data=d;
            next=NULL;
        }
};

class Solution{
    public:
    Node* insert(Node *head,int data)
    {
        Node *new_node = new Node(data);
        if (head == NULL)
        {
            return new_node;
        }
        else
        {
            Node* last = head;
            while (last->next != NULL)
            {
                last = last->next;
            }
            last->next = new_node;
            return head;
        }
    }

      void display(Node *head)
      {
          Node *start=head;
          while(start)
          {
              cout<<start->data<<" ";
              start=start->next;
          }
          cout <<"\n";
      }
};

int main()
{
    Node* head=NULL;
      Solution mylist;
    int T,data;
    cin>>T;
    while(T-->0){
        cin>>data;
        head=mylist.insert(head,data);
    }
    mylist.display(head);
    return 0;
}
