/*
 * Round the value to nearest integer
 * Floor the value to largest integer that is smaller than it
 * Ceil the value to smallest integer that is larger that it
 * Power of a number
 *
**/

#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    double var_d1 = 15.36;
    double var_d2 = 15.86;
    int base=0, exponent=0;

    cout << "Round " << to_string(var_d1) << ": " << round(var_d1) <<endl;
    cout << "Round " << to_string(var_d2) << ": " << round(var_d2) <<endl;
    cout << "Floor " << to_string(var_d1) << ": " << floor(var_d1) <<endl;
    cout << "Floor " << to_string(var_d2) << ": " << floor(var_d2) <<endl;
    cout << "Ceil " << to_string(var_d1) << ": " << ceil(var_d1) <<endl;
    cout << "Ceil " << to_string(var_d2) << ": " << ceil(var_d2) <<endl;

    cout << "Enter base number: ";
    cin >> base;
    cout << "Enter exponent number: ";
    cin >> exponent;
    cout << base << " to " << exponent << " is " << pow(base, exponent) <<endl;

    return 0;
}
