/*
 * Pointers
 *
 * Ref: https://www.udacity.com/course/c-for-programmers--ud210
**/

#include <iostream>

int main()
{
    int a = 54;
    int *ptr_a = &a;
    std::cout<<"a = "<<a<<"\n";
    std::cout<<"address of a is at &a = "<< &a<<"\n\n";

    std::cout<<"value of a is at *ptr_a = "<< *ptr_a<<"\n";
    std::cout<<"address of a is at ptr_a = "<< ptr_a<<"\n";
    std::cout<<"address of ptr_a = "<< &ptr_a<<"\n\n";


    int * pointerI;
    int number;
    char character;
    char * pointerC;
    std::string sentence;
    std::string *pointerS;

    pointerI = &number;
    *pointerI = 45;

    pointerC = &character;
    *pointerC = 'f';

    pointerS = &sentence;
    *pointerS = "Hey look at me, I know pointers!";

    std::cout << "number = "<<number<<"\n";
    std::cout<<"character = "<<character<<"\n";
    std::cout<<"sentence = "<<sentence<<"\n";

    return 0;
}
