/*
 * Dynamically allocated double sized array
 *
 * Ref: https://www.hackerrank.com/challenges/variable-sized-arrays/
**/

#include <iostream>
using namespace std;


int main()
{
    int num_of_arrays = 0;
    int num_of_queries = 0;
    int **arrays;

    // Firstly determine the number of arrays
    cout << "Enter the number of arrays: ";
    cin >> num_of_arrays;
    cin.ignore(100, '\n');
    arrays = (int**) malloc(num_of_arrays * sizeof(int*));

    for (int i=0; i<num_of_arrays; i++)
    {
        // First input is the size, others are elements of that array
        cout << "Enter the size of the array-" << i << ": ";
        int array_size;
        cin >> array_size;
        cin.ignore(100, '\n');

        // First element is the size
        arrays[i] = (int*) malloc((array_size+1) * sizeof(int));
        arrays[i][0] = array_size;

        cout << "Enter the elements seperated by spaces: ";
        for (int j=1; j<array_size+1; j++)
        {
            cin >> arrays[i][j];
        }
        cin.ignore(100, '\n');
    }
    
    // Print the arrays
    for (int i=0; i<num_of_arrays; i++)
    {
        cout << "\nArray-" << i << ":";
        for (int j=1; j < arrays[i][0]+1; j++)
        {
            cout << " " << arrays[i][j];
        }
    }
    cout << "\n";

    return 0;
}
