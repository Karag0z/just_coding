/*
 * Basic input output functions
 *
 * Ref: https://www.hackerrank.com/challenges/30-data-types/problem
 *      https://www.udacity.com/course/c-for-programmers--ud210
**/

#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    // Variable declarations
    int var_i = 0;
    double var_d = 0.0;
    string var_s = "";
    char arr_c[128];

    cout << "Enter an integer and double: ";
    cin >> var_i >> var_d;
    cin.ignore(100,'\n');       // cin.ignore is required to empty buffer of cin
    cout << "Enter a string: ";
    getline(cin, var_s);        // Whole line, for strings
    cout << "Enter a char array: ";
    cin.getline(arr_c, 128);    // Whole line, for char arrays

    // Print them
    cout.precision(1);              // It is used to set precision for doubles.
    cout << var_i << endl;
    cout << fixed << var_d << endl; // "fixed" is required to show the precision
    cout << var_s << endl;
    cout << arr_c << endl;

    /* ---------------------------------------------------------------------- */
    // Format output
    cout << "\n\nThe text without any formating\n";
    cout << "Ints" << "Floats" << "Doubles" << "\n";

    cout << "\nThe text with setw(15)\n";
    // Ints         Floats        Doubles
    //     <-- 15      --><-- 15      -->
    cout << "Ints" << setw(15) << "Floats" << setw(15) << "Doubles" << "\n";

    cout << "\nThe text with tabs\n";
    cout << "Ints\t" << "Floats\t" << "Doubles" << "\n";

    return 0;
}
