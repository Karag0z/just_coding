/*
 * Basic string operations
 *
**/

#include <iostream>

using namespace std;

int main()
{
    string var_a;
    string var_b;
    string even_part;
    string odd_part;

    cout << "String 1: ";
    cin >> var_a;
    cout << "String 2: ";
    cin >> var_b;

    // String concatenation
    cout << "Concatenation: " << var_a + var_b <<endl;

    // Size of strings
    // TODO: Non-ascii characters cause wrong size
    cout << "Sizes: " << var_a.size() << " " << var_b.size() <<endl;

    // The parameters of swap() can be of any data type
    swap(var_a[0], var_b[0]);
    cout << "Swapped first characters: " << var_a << " " << var_b <<endl;

    // substr(pos, length_of_substring)
    cout << "First half of the string: " << var_a.substr(0, var_a.size()/2) <<endl;
    cout << "First half of the string: " << var_b.substr(0, var_b.size()/2) <<endl;

    // insert(pos, string)
    for (int i=0; i<var_a.size(); i++)
    {
        if (0 == i%2)
        {
            even_part.insert(i/2, var_a.substr(i, 1));
        }
        else
        {   // Index is typecasted to a floor integer, e.g. (int)3/2 is 1
            odd_part.insert(i/2, var_a.substr(i, 1));
        }
    }
    cout << "Even and odd indexed chars: " << even_part << " " << odd_part <<endl;

    return 0;
}
