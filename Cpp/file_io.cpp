/*
 * Read from and write to a file
 *
 * Ref: https://www.udacity.com/course/c-for-programmers--ud210
**/

#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    // Variable declaration
    string line;

    // Create an output stream to write to the file
    // Add new lines to the end of the file
    ofstream myfileI ("input.txt");
    // ofstream myfileI ("input.txt", ios_base::app);   // It is for appending
    if (myfileI.is_open())
    {
        myfileI << "\nI am adding a line.\n";
        myfileI << "I am adding another line.\n";
        myfileI.close();
    }
    else
    {
        cout << "Unable to open file for writing";
    }

    //create an input stream to write to the file
    ifstream myfileO ("input.txt");
    if (myfileO.is_open())
    {
        while ( getline (myfileO,line) )
        {
            cout << line << '\n';
        }
        myfileO.close();
    }
    else
    {
        cout << "Unable to open file for reading";
    }
    return 0;
}
