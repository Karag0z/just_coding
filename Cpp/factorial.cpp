/*
 * Recursion
 * Factorial of a number
 *
**/

#include <iostream>

using namespace std;

int factorial(int n)
{
    if (n <= 1)
    {   // Base condition to end recursion
        return 1;
    }
    else
    {   // Call function itself
        return n*factorial(n-1);
    }
}

int main()
{
    int n;
    cout << "Enter the number: ";
    cin >> n; 

    int result = factorial(n);
    cout << "Factorial of " << n << " is: " << result <<endl;

    return 0;
}
