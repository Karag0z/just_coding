/*
 * Number guess with random number generator
 *
 * Ref: https://www.udacity.com/course/c-for-programmers--ud210
 *      https://www.cplusplus.com/reference/cstdlib/rand/
**/

#include <iostream>
#include <sstream>
#include <time.h> //added for the random number generator seed
#include <cstdlib>//added to use the rand function

const int MIN_GUESS = 1;
const int MAX_GUESS = 100;

int main()
{
    int target;
    std::string userString;
    int guess = -1;

    srand(time(NULL));  //set the seed for the random number generator
    // How to set min and max limits
    // v1 = rand() % 100;         // v1 in the range 0 to 99
    // v2 = rand() % 100 + 1;     // v2 in the range 1 to 100
    // v3 = rand() % 30 + 1985;   // v3 in the range 1985-2014
    target = rand() % (MAX_GUESS+MIN_GUESS-1) + MIN_GUESS; //generate the 'random' number

    while(guess != target)
    {
        std::cout<<"Guess a number between 1 and 100: ";
        std::getline (std::cin,userString);
        //convert to an int
        std::stringstream(userString) >> guess;
        std::cout<<userString<<"\n";
        if(guess > target)
            std::cout<<"Your guess is too high\n";
        else if(guess < target)
            std::cout<<"Your guess is too low\n";
        else
            std::cout<<"You guessed the target!\n";

        //Note I had to use double quotes around "q"
        //because it is a string
        if(userString == "q")
        {
            std::cout<<"good bye!";
            std::cout<<"The number was "<<target<<"\n";
            break;
        }
    }
    return 0;
}
