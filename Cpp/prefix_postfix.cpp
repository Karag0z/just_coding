/*
 * Prefix and Postfix operators
 *
 * Ref: https://www.udacity.com/course/c-for-programmers--ud210
**/

#include<iostream>

using namespace std;

int main()
{
    int a, b = 0;
    int post, pre = 0;
    cout<<"Inital values: \t\t\tpost = "<<post<<" pre= "<<pre<<"\n";

    // Assign value of "a" to "post", and then increment the value of "a"
    post = a++;
    // Increment the value of "b", and then assign value of "b" to "pre"
    pre = ++b;
    cout<<"After one postfix and prefix: \tpost = "<<post<<" pre= "<<pre<<"\n";

    post = a++;
    pre = ++b;
    cout<<"After two postfix and prefix: \tpost = "<<post<<" pre= "<<pre<<"\n";  
    return 0;
}
