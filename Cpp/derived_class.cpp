/*
 * Inheritance, base class, derived class
 *
 * Ref: https://www.udacity.com/course/c-for-programmers--ud210
 *      https://www.codegrepper.com/code-examples/cpp/c%2B%2B+inheritance+constructor
**/

#include <iostream>

using namespace std;

class Animal
{
private:
    int age;
    string environment;
public:     // Public ones can be accessed by derived classes
    Animal(int ageIn, string envIn);
    void setAge(int ageIn);
    int getAge();
    void setEnvironment(string envIn);
    string getEnvironment();
};

Animal::Animal(int ageIn, string envIn)
{
    age = ageIn;
    environment = envIn;
}
void Animal::setAge(int ageIn)
{
    age = ageIn;
}
int Animal::getAge()
{
    return age;
}
void Animal::setEnvironment(string envIn)
{
    environment = envIn;
}
string Animal::getEnvironment()
{
    return environment;
}

class Cat : public Animal
{
private:
    string name;
public:
    Cat(string nameIn, int ageIn, string envIn);
    void setName(string nameIn);
    string getName();
    void printInfo();
};

// If the base class requires parameters for its constructor
Cat::Cat(string nameIn, int ageIn, string envIn)
    : Animal(ageIn, envIn)
{
    name = nameIn;
}
void Cat::setName(string nameIn)
{
    name = nameIn;
}
string Cat::getName()
{
    return name;
}
void Cat::printInfo()
{
    cout<< name <<" lives on "<< getEnvironment();
    cout<<" for "<< getAge() <<" years\n";
}

int main()
{
    Cat cat1("Pamuk", 4, "Land");
    cat1.printInfo();

    return 0;
}
