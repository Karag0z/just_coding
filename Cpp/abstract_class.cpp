/*
 * Inheritance, base class, derived class
 *
 * Ref: https://www.geeksforgeeks.org/pure-virtual-functions-and-abstract-classes/
**/

#include <iostream>
using namespace std;

// Any class with virtual function is an abstract class
class Shape
{
protected:
    int id;
public:
    Shape(int idIn)
    {
        id = idIn;
    }
    /* We will override the virtual function in derived class
     * If virtual function is not overriden, derived class becomes abstract
     * Virtual function must have definition, declaration is not enough.
     */ 
    virtual void printInfo() = 0;
};

class Square : public Shape
{
private:
    int edge;
    int area;
public:
    Square(int idIn, int edgeIn)
    : Shape(idIn)
    {
        edge = edgeIn;
        area = edge*edge;
    }
    void printInfo()    // Override virtual function
    {
        cout << "Id: "<< id <<"\n";
        cout << "Edge: "<< edge <<"\n";
        cout << "Area: "<< edge*edge <<"\n";
    }
};

int main()
{
    int id;
    int edge;

    cout << "Id: ";
    cin >> id;
    cout << "Edge: ";
    cin >> edge;

    cout << "\n";
    Square sq1(id, edge);
    sq1.printInfo();

    return 0;
}
