/*
 * Map, dictionaries
 *
 * Ref: https://stackoverflow.com/a/54584048
**/

#include <iostream>
#include <map>

using namespace std;

int main()
{
    // Variable declarations
    map<string, int> phonebook;
    int num_of_entries;

    cout << "Number of entries: ";
    cin >> num_of_entries;

    for (int i=0; i<num_of_entries; i++)
    {
        string name;
        int phone_number;

        cout << "Enter name: ";
        cin >> name;
        cin.ignore(100,'\n');

        cout << "Phone number: ";
        cin >> phone_number;
        cin.ignore(100,'\n');

        // Dictionary can be updeted by one of the following lines
        // phonebook.insert(pair<string, int>(name, phone_number));
        phonebook[name] = phone_number;
    }

    // Print the phonebook
    for (const auto& p : phonebook )
    {
        std::cout << p.first << " \t- " << p.second << std::endl;
    }

    return 0;
}
