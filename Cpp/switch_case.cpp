/*
 * Switch-case with and without breaks
 * Ternary operator, one line if-else
 *
 * Ref: https://www.hackerrank.com/challenges/c-tutorial-conditional-if-else/problem
 *      https://www.udacity.com/course/c-for-programmers--ud210
**/

#include <iostream>

using namespace std;

int main()
{
    int var_i = 0;

    cout << "Enter a single digit number: ";
    cin >> var_i;

    switch (var_i)
    {
        case 0:
            cout << "zero" << endl;
            break;
        case 1:
            cout << "one" << endl;
            break;
        case 2:
            cout << "two" << endl;
            break;
        case 3:
            cout << "three" << endl;
            break;
        case 4:
            cout << "four" << endl;
            break;
        case 5:
            cout << "five" << endl;
            break;
        case 6:
            cout << "six" << endl;
            break;
        case 7:
            cout << "seven" << endl;
            break;
        case 8:
            cout << "eight" << endl;
            break;
        case 9:
            cout << "nine" << endl;
            break;
        default:
            cout << "Other than a single digit number" << endl;
            // Ternary operator, print even or odd
            cout << "That is " << ((0 == var_i % 2) ? "even":"odd") << endl;
    }

    // If there is no "break;", remaining cases will be executed
    char begin;
    std::cout<<"\n\nWhere do you want to begin?\n";
    std::cout<<"B. At the beginning?\nM. At the middle?";
    std::cout<<"\nE. At the end?\n\n";
    cin >> begin;

    switch (begin)
    {
        case('B'): std::cout<<"Once upon a time there was a wolf.\n";
        case('M'): std::cout<<"The wolf hurt his leg.\n";
        case('E'): 
            std::cout<<"The wolf lived happily everafter\n";
            break;  // This break is used not to execute default case
        default: std::cout<<"Choose \'B\',\'M\' or \'E\'\n";
    }

    return 0;
}
