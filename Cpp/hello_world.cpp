/*
 * Hello world from C and C++, data types, constants
 *
**/

#include <iostream>

using namespace std;

int main()
{
    // Let's start with greetings!
    printf("(printf)Hello, World!\n");      // C
    cout << "(cout)Hello, world!" << endl;  // C++, "\n" can be used here

    // Variable types and sizes
    cout<<"bool   size = "<<sizeof(bool)<<"\n";     // bool   size = 1
    cout<<"char   size = "<<sizeof(char)<<"\n";     // char   size = 1
    cout<<"short  size = "<<sizeof(short)<<"\n";    // short  size = 2
    cout<<"int    size = "<<sizeof(int)<<"\n";      // int    size = 4
    cout<<"float  size = "<<sizeof(float)<<"\n";    // float  size = 4
    cout<<"long   size = "<<sizeof(long)<<"\n";     // long   size = 8
    cout<<"double size = "<<sizeof(double)<<"\n";   // double size = 8
    cout<<"string size = "<<sizeof(string)<<"\n";   // string size = 32

    // Value of the constant is same for the life of the program
    const int weightGoal = 100;
    // weightGoal = 234;    // Not possible to assign a value to a constant

    cout << endl;
    // Enums start with index 0
    enum MONTH {Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec};
    MONTH current_month = Jun;
    if (6 == (int)current_month)
    {
        cout << "Jun is the sixth month" << endl;
    }
    else
    {
        cout << "What is wrong? First element of enum is 0, not 1" << endl;
    }

    // It is enough to set first one. Others are incremented by 1
    enum DAY {Mon=1, Tue, Wed, Thu, Fri, Sat, Sun};
    DAY fixed_enum = Sat;
    if (6 == (int)fixed_enum)
    {
        cout << "Saturday is the sixth day" << endl;
    }
    else
    {
        cout << "What is wrong?" << endl;
    }

    return 0;
}
